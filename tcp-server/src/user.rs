/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/




extern crate lazy_static;


use self::lazy_static::*;
use std::default::Default;
use std::sync::Mutex;




#[derive(Default)]
pub struct User {

    pub username : String,
    pub session_key : String,
    pub email : String,
    pub hidden_msg: String,
    pub active : bool,
}


lazy_static! {

    static ref USER: Mutex<User> = {
        let u = Mutex::new(User::default());
        u
    };
}



pub fn set_username( username:String ) {

    USER.lock().unwrap().username = username;
}


pub fn set_email( email:String ) {

    USER.lock().unwrap().email = email;
}


pub fn set_session_key( session_key: String ) {

    USER.lock().unwrap().session_key = session_key;
}


pub fn set_user_status ( active: bool ) {

    USER.lock().unwrap().active = active;
}

pub fn set_hidden_msg( hidden_msg:String ) {

    USER.lock().unwrap().hidden_msg = hidden_msg;
}


pub fn get_username() -> String {

    let username = USER.lock().unwrap().username.clone();
    username
}

pub fn get_session_key() -> String {

    let session_key = USER.lock().unwrap().session_key.clone();
    session_key
}

pub fn get_hidden() -> String {

    let hidden_msg = USER.lock().unwrap().hidden_msg.clone();
    hidden_msg
}

//pub fn get_user_status() -> bool {
//    let status = USER.lock().unwrap().active.clone();
//    status
//}
