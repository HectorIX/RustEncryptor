/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/





use encryptor_services_list;
use parser;


pub fn state( client_request:String ) -> String {

    let state_tupple = parser::request_splitter(client_request);

    let the_state = state_tupple.0;
    let the_request = state_tupple.1;

    println!("state = {:?}", the_state );
    println!("the_request = {:?}", the_request);

    match the_state.as_ref() {

        "informatic_state" => {

            encryptor_services_list::service_list(the_request)
        },
        "sign_up_state" => {

            encryptor_services_list::service_list(the_request)
        },
        "sign_in_state" => {

            encryptor_services_list::service_list(the_request)
        }
        "upload_state" => {

            encryptor_services_list::service_list(the_request)
        },
        "download_state" => {

            encryptor_services_list::service_list(the_request)
        },
        _ => {

            "Undefined State...".to_string()
        },

    }
}
