/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/




/******************************************

    RUN BY TYPING: cargo run 


*******************************************/

extern crate bytes;
extern crate futures;
extern crate tokio_io;
extern crate tokio_proto;
extern crate tokio_service;


use tokio_proto::TcpServer;

mod service_state;
mod encryptor_services_list;
mod parser;
mod user;
mod codec;
mod file_io;
mod sign_up;
mod sign_in;
mod upload;
mod download;
mod generate_password;


fn main() {
    // Specify the localhost address
    let addr = "127.0.0.1:4000".parse().unwrap();

    // The builder requires a protocol and an address
    let server = TcpServer::new(codec::LineProto, addr);
    println!("Listening on http://{}", addr);
    // We provide a way to *instantiate* the service for each new
    // connection; here, we just immediately return a new instance.
    server.serve(|| Ok(codec::Encryptor));

}
