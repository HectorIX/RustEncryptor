/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/





use file_io;
use parser;
use user;


pub fn download_service( data: String ) -> String {


    let mut path = "private_data/".to_string();


    if data.len() > 0 {

        let (username, rest_data) = parser::split_credentials(data);
        let (session_key, filename) = parser::split_session_key(rest_data);


        path.push_str(&username);
        path.push_str("/");
        path.push_str(&filename);

        if (username == user::get_username()) &
           (session_key == user::get_session_key()) {


            let mut file_request = "download_state::OK**".to_string();
            let file_context = file_io::read_file(path.to_string());


            if !file_context.starts_with("**Failed") {

                file_request.push_str(&user::get_session_key());
                file_request.push_str("#!?#");
                file_request.push_str(&filename);
                file_request.push_str("<$$>");
                file_request.push_str(&file_context);

            }
            else {

                file_request = "download_state::Failure**".to_string();
            }

            file_request

        }
        else {

            "download_state::SESSION_Expired**".to_string()
        }
    }
    else {

        "download_state::Failed**".to_string()
    }

}
