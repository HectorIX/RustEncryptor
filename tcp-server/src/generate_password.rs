/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/




extern crate rand;

use self::rand::{Rng, thread_rng};


pub fn random_password( length:usize ) -> String {


    let mut password = String::new();
    let mut rng = thread_rng();


    for _ in 0..length {

        match rng.gen_range(0, 5) {

            // Generate special characters from '!' to '/'
            0 => password.push(rng.gen_range(33, 48) as u8 as char),

            // Generate Numbers from 0 to 9
            1 => password.push(rng.gen_range(48, 58) as u8 as char),

            // Generate special characters from ':' to '@'
            2 => password.push(rng.gen_range(58, 65) as u8 as char),

            // Generate uppercase letters from A to Z
            3 => password.push(rng.gen_range(65, 91) as u8 as char),

            // Generate lowercase letters from a to z
            4 => password.push(rng.gen_range(97, 123) as u8 as char),

            _ => println!("Unreachable state!"),
        }
    }


    password

}
