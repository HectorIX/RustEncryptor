/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/





use sign_up;
use sign_in;
use upload;
use download;
use parser;



pub fn service_list( request_service:String ) -> String {

    let (service, data) = parser::data_splitter(request_service);


    println!("service = {:?}", service );
    println!("data = {:?}", data );


    match service.as_ref() {

        "Sign-up" => {

            sign_up::sign_up_service(data)
        },
        "Sign-in" => {

            sign_in::sign_in_service(data)
        },
        "TwoFactor" => {
            println!("I am here" );
            sign_in::two_factor( data )
        },
        "Upload" => {

            upload::upload_service(data)
        },
        "Download" => {

            download::download_service(data)
        },
        _ => {

            "A typpo error! Please try again...\n[Tip: type help]\n".to_string()
        },


    }
}
