/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/





extern crate crypto;
extern crate rand;
extern crate sendmail;



use parser;
use file_io;
use user;
use generate_password;

use std::str;

use self::rand::{ thread_rng, Rng };
use self::crypto::whirlpool::Whirlpool;
use self::crypto::digest::Digest;
use self::sendmail::email;



pub fn sign_in_service(credentials:String) -> String {


    if credentials.len() > 0 {

        let path = "database/user-credentials.txt";
        let (username, password) = parser::split_credentials(credentials);


        if verify_user(path.to_string(), username.clone(), password) {


            let hidden_msg = generate_password::random_password(10);

            println!("hidden_msg = {}",  hidden_msg.clone());
            user::set_username(username);
            user::set_hidden_msg(hidden_msg.clone());
            user::set_session_key(session_key_maker());
            user::set_user_status(true);

            send_email(hidden_msg.clone());


            format!("sign_in_state::First_Step_OK**")

        }
        else {

            format!("sign_in_state::NOT_Mactching**")
        }
    }
    else {

        format!("sign_in_state::ALREADY_Sign_in**")
    }

}


pub fn two_factor( message:String ) -> String {

    println!("Hidden Message = {:?}", user::get_hidden());

    if message == user::get_hidden() {

        user::set_user_status(true);

        format!("sign_in_state::OK**{}--{}", user::get_session_key()
                                           , user::get_username())


    }
    else {

        format!("sign_in_state::NOT_Mactching**")

    }


}


fn send_email(message:String) {

    // Configure email body and header
    email::create(
        // From Address
        "hectorix00@gmail.com",
        // To Address
        "giorgoschiotis@yandex.com",
        // Subject
        &message,
        // Body
        "<html><body><h1>I am the body. Hello Wolrd!<br/><br/>And I accept html.</h1></body></html>"
    );

    // Define the actual email address to receive the email
    email::send("giorgoschiotis@yandex.com");
    println!("Email send successfully!");
}



fn verify_user( path:String, username:String, password:String ) -> bool {

    let file_context:String = file_io::read_file(path);
    let vector_of_users: Vec<&str> = file_context.split("<**>\n").collect();


    for user_data in vector_of_users {


        if user_data.to_string().contains(username.as_str()) {

            let legitimate_password = return_pass(user_data.to_string());

            match password {

                _ if legitimate_password == password =>  { return true; }
                _ => {return false;}
            }
        }
    }

    false
}


fn return_pass(user_data:String) -> String {

    let v:Vec<&str> = user_data.split("password::").collect();
    let v_plus = v[1].to_string();
    let v_minus:Vec<&str> = v_plus.split("\nID::").collect();

    let password:String = v_minus[0].to_string();

    password

}


fn session_key_maker() -> String {

    let mut whirlpool_hasher = Whirlpool::new();
    let random_key: String = thread_rng().gen_ascii_chars()
                                         .take(128)
                                         .collect();


    whirlpool_hasher.input_str(&random_key);
    let session_key = whirlpool_hasher.result_str();

    session_key

}
