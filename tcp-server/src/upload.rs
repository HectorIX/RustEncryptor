/************************************************************************

Copyright [2017] [Georgios Chiotis]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*************************************************************************/




use file_io;
use parser;
use user;



pub fn upload_service(data:String) -> String {

    let mut path = "private_data/".to_string();


    if data.len() > 0 {

        let (username, rest_data) = parser::split_credentials(data);
        let (session_key, file_data) = parser::split_session_key(rest_data.clone());
        let (filename, file_context) = parser::split_filename_from_context(file_data.clone());


        path.push_str(&username);
        path.push_str("/");
        path.push_str(&filename);

        if (username == user::get_username()) &
           (session_key == user::get_session_key()) {

            let up = file_io::write_file(path, file_context);

            if !up.starts_with("**Failed") {

                "upload_state::OK**".to_string()
            }
            else {

                "upload_state::Failure**".to_string()
            }

         }
         else {

             "upload_state::SESSION_Expired**".to_string()
         }


    }
    else {

        "upload_state::Failed**".to_string()
    }

}
